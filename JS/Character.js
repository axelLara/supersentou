import * as THREE from 'https://cdn.jsdelivr.net/npm/three@0.118/build/three.module.js';

import {FBXLoader} from 'https://cdn.jsdelivr.net/npm/three@0.118.1/examples/jsm/loaders/FBXLoader.js';
import {Physics} from "./Physics.js";
import {Move} from "./Moves.js";


class FBXLoading {

    constructor(){

        this.parent = new THREE.Object3D();
        this.parent.position.x = 0;
        this.LoadModel();
    }

    /*
    * Aquí se cargan el modelo en t-pose y las animaciones.
    * */

    LoadModel() {
        let loader = new FBXLoader();
        loader.setPath('./assets/Models/Character/Animaciones/');
        loader.load('monito-con-rig.fbx', (fbx) => {
            fbx.scale.setScalar(0.05);

            fbx.traverse(c => {
                c.castShadow = true;
            });



            const anim = new FBXLoader();
            anim.setPath('./assets/Models/Character/Animaciones/');
            anim.load('Idle.fbx', (anim) => {
                this.isLoaderReady[0] = true;
                const m = new THREE.AnimationMixer(fbx);
                this.idleMixers.push(m);
                const idle = m.clipAction(anim.animations[0]);

                this.AnimationActions.push(idle);
            });



            anim.load('Walking_forward2.fbx', (anim) => {
                this.isLoaderReady[1] = true;
                const m = new THREE.AnimationMixer(fbx);
                this.forwardMixers.push(m);
                const idle = m.clipAction(anim.animations[0]);

                this.AnimationActions.push(idle);
            });




            anim.load('Walking_backward2.fbx', (anim) => {
                this.isLoaderReady[2] = true;
                const m = new THREE.AnimationMixer(fbx);
                this.backwardsMixers.push(m);
                const idle = m.clipAction(anim.animations[0]);
                this.AnimationActions.push(idle);
            });



            anim.load('Down_Start.fbx', (anim) => {
                this.isLoaderReady[3] = true;
                const m = new THREE.AnimationMixer(fbx);
                this.bendMixers.push(m);
                const idle = m.clipAction(anim.animations[0]);
                idle.clampWhenFinished = true;
                idle.loop = THREE.LoopOnce;
                this.AnimationActions.push(idle);
            });


            anim.load('Punch2.fbx', (anim) => {
                this.isLoaderReady[4] = true;
                const m = new THREE.AnimationMixer(fbx);
                this.punchMixers.push(m);
                const idle = m.clipAction(anim.animations[0]);
                idle.loop = THREE.LoopOnce;
                this.AnimationActions.push(idle);


            });

            anim.load('Jump.fbx', (anim) => {
                this.isLoaderReady[5] = true;
                const m = new THREE.AnimationMixer(fbx);
                this.jumpMixers.push(m);
                const idle = m.clipAction(anim.animations[0]);
                idle.loop = THREE.LoopOnce;
                idle.clampWhenFinished = true;
                this.AnimationActions.push(idle);


            });


            anim.load('Punch_hard.fbx', (anim) => {
                this.isLoaderReady[6] = true;
                const m = new THREE.AnimationMixer(fbx);
                this.punchHardMixers.push(m);
                const idle = m.clipAction(anim.animations[0]);
                idle.loop = false;

                this.AnimationActions.push(idle);
                idle.clampWhenFinished = true;


            });







            this.parent.add(fbx);



        });


    }


    Render(deltaTime, animation) {

        switch(animation){

            case 0:{
                if (this.idleMixers) {
                    if(this.AnimationActions!==null&&this.AnimationActions!==undefined&&this.AnimationActions.length>0){

                        this.AnimationActions[0].play();
                    }
                    this.idleMixers.map((m )=> m.update(deltaTime));
                }
                break;
            }
            case 1:{

                if (this.forwardMixers) {
                    if(this.AnimationActions!==null&&this.AnimationActions!==undefined&&this.AnimationActions.length>0){

                        this.AnimationActions[1].play();
                    }
                    this.forwardMixers.map((m )=> m.update(deltaTime));
                }
                break;
            }

            case 2:{

                if (this.backwardsMixers) {
                    if(this.AnimationActions!==null&&this.AnimationActions!==undefined&&this.AnimationActions.length>0){

                        this.AnimationActions[2].play();
                    }
                    this.backwardsMixers.map((m )=> m.update(deltaTime));

                }
                break;
            }

            case 3:{

                if (this.bendMixers) {
                    if(this.AnimationActions!==null&&this.AnimationActions!==undefined&&this.AnimationActions.length>0){

                        this.AnimationActions[3].play();
                    }
                    this.bendMixers.map((m )=> m.update(deltaTime*3));
                }
                break;
            }

            case 4:{

                if (this.punchMixers) {
                    if(this.AnimationActions!==null&&this.AnimationActions!==undefined&&this.AnimationActions.length>0){

                        this.AnimationActions[4].play();
                    }
                    this.punchMixers.map((m )=> m.update(deltaTime*2.5));
                }
                break;
            }

            case 5:{

                if (this.jumpMixers) {
                    if(this.AnimationActions!==null&&this.AnimationActions!==undefined&&this.AnimationActions.length>0){

                        this.AnimationActions[5].play();
                    }
                    this.jumpMixers.map((m )=> m.update(deltaTime));
                }
                break;
            }

            case 6:{

                if (this.punchHardMixers) {
                    if(this.AnimationActions!==null&&this.AnimationActions!==undefined&&this.AnimationActions.length>0){

                        this.AnimationActions[6].play();
                    }
                    this.punchHardMixers.map((m )=> m.update(deltaTime*2.5));
                }
                break;
            }

        }


    }


    LoaderReady() {

        return(this.isLoaderReady[0] && this.isLoaderReady[1] && this.isLoaderReady[2]
            && this.isLoaderReady[3] && this.isLoaderReady[4] && this.isLoaderReady[5]
            && this.isLoaderReady[6]);

    }

    idleMixers = [];
    forwardMixers = [];
    backwardsMixers = [];
    bendMixers = [];
    punchMixers = [];
    jumpMixers = [];
    punchHardMixers = [];


    isLoaderReady = [false, false, false, false, false, false, false];


    AnimationActions = [];
    parent;
}


export class Character{


    Render(deltaTime, stage){


        //Validador de la animacion idle. Si no se ejecuta ninguna otra, tiene que ser esa.
        if(!this.moveLeft && !this.moveRight && !this.bend && !this.punch &&!this.punchHard
        && !this.physicsHandler.jumpingFlag){
            this.animation = 0;
        }

        //Si está saltando, evitar las animaciones de caminar y agacharse
        if(this.physicsHandler.jumpingFlag){
            if(this.moveRight || this.moveLeft || this.bend){

                this.animation = 5;

            }

        }

        //Se envia a la funcion render del modelo la animacion que corresponda
        this.loader.Render(deltaTime, this.animation);

        /*
            Se calcula la colision con el suelo y se envia a la funcion update de physics handler, que regresa la
            posicion en y con efecto de la aceleracion de la gravedad y la normal.
        * */
        this.rayCaster.set(this.loader.parent.position,this.downRay);  //enviamos la posicion del modelo y una recta hacia abajo

        let colision = this.rayCaster.intersectObject(stage, true); //nos devuelve los datos de la colision

        let collides = false;  //Si esta variable es true, colisiona

        if(colision.length > 0 && colision[0].distance <0.01) {  //si la colision existe y es menor a 0.01
            collides = true;

        }
        this.loader.parent.position.y = this.physicsHandler.Update(deltaTime,
            collides);  //envio de la variable collides al update de physics y se iguala su respuesta a la posicion de y



        this.MovementX(deltaTime);  //Hace los movimientos en x por input del jugador



        this.Animate(deltaTime);    //Se encarga de ejecutar las animaciones
    }


    Animate(deltaTime){


        if(this.moveLeft){

            this.animation = 1;

        }

        if(this.moveRight){

            this.animation = 2;

        }

        if(this.bend){

            this.animation = 3;


        }

        if(this.punch){

            this.animation = 4;



        }

        if(this.jump){

            this.physicsHandler.UpwardsImpulse(deltaTime);

        }

        if (this.physicsHandler.jumpingFlag){
            this.animation = 5;
        }

        if(this.punchHard){

            this.animation = 6;

        }

        if(this.kick){

            this.animation = 7;

        }

        if(this.kickHard){
            this.animation = 8;
        }






    }

    Blow(){
        this.onHitting = false;
        switch(this.move){
            case 0:{ return new Move(0.6, 10,0.02); }

            case 1: { return new Move(0.8, 40, 0.08); }
        }

    }

    ReceiveBlow(move){

        if(!this.bend && !this.punchHard){

            this.physicsHandler.SetInitialAcceleration(move.push);

            this.lifePercentage -= move.damage;

        }


    }

    MovementX(deltaTime){

        if(this.loader.parent.position.x>1.7605){

            this.loader.parent.position.x = 1.76049;

        }

        if(this.loader.parent.position.x<-1.7604){

            this.loader.parent.position.x = -1.76039;

        }

        if(this.moveLeft){


            this.loader.parent.position.x+= 0.8 * deltaTime;

        }

        if(this.moveRight){

            this.loader.parent.position.x-= 0.8 * deltaTime;

        }

        this.physicsHandler.PushX(deltaTime);
        this.loader.parent.position.x+=this.physicsHandler.x;


    }

    LookLeft(){

        if(this.loader.parent.scale.x <0) {

            this.loader.parent.scale.x *= -1;

        }
    }
    LookRight(){

        if(this.loader.parent.scale.x >0) {

            this.loader.parent.scale.x *= -1;

        }
    }


    constructor(index){

        this.loader = new FBXLoading();         //Inicia el modelo y sus animaciones. Tarda una BARBARIDAD


        let idInterval = setInterval(()=>{
            if(this.loader.LoaderReady()){

                clearInterval(idInterval);

                this.physicsHandler = new Physics();    //Inicia el manejador de fisicas
                this.physicsHandler.y = 0;
                this.rayCaster = new THREE.Raycaster();

                this.downRay = new THREE.Vector3(0,-1,0);   //Recta hacia abajo del personaje para validar el suelo.

                this.lifePercentage = 100;      //Vida del personaje, inicia en 100

                this.animation = 0;            //Iniciamos con la animacion idle

                this.onHitting = false;
                this.onReceiveHit =false;



                this.index = index;            //Indice del personaje, si es el jugador 1:0 si es el jugador 2: 1
                //Si cambian de lado cambian de indice

                /*Controles de los personajes segun el indice*/
                if(index === 0){

                        this.loader.AnimationActions[5].getMixer().addEventListener( 'finished', ()=>{


                            this.loader.AnimationActions[5].reset();

                        } );


                        this.loader.AnimationActions[4].getMixer().addEventListener( 'finished', ()=>{

                            this.punch = false;

                            this.loader.AnimationActions[4].reset();
                        } );




                    document.addEventListener('keydown', ($event)=>{

                        if($event.key==='d') {

                            if (!this.bend){

                                this.moveLeft = true;

                            }


                        }



                        if($event.key==='a') {

                            if(!this.bend){

                                this.moveRight = true;

                            }



                        }

                        if ($event.key === 'w') {

                            if(!this.bend){

                                this.jump = true;

                            }



                        }

                        if ($event.key === 's') {

                            this.bend = true;


                        }

                        if ($event.key === 'j') {


                            if(!this.punch && !this.bend){
                                this.loader.AnimationActions[4].paused=false;
                                this.loader.AnimationActions[4].time = 0;
                                this.punch = true;

                                if(!this.onHitting){

                                    this.onHitting = true;
                                    this.move = 0;

                                }


                                setTimeout(()=>{
                                    this.loader.AnimationActions[4].paused=true;
                                    this.punch= false;
                                    this.onHitting = false;
                                },300);
                            }




                        }

                        if ($event.key === 'i') {

                            if(!this.punchHard && !this.bend){
                                this.loader.AnimationActions[6].paused=false;
                                this.loader.AnimationActions[6].time = 0;
                                this.punchHard = true;

                                if(!this.onHitting){

                                    setTimeout(()=>{

                                        this.onHitting = true;
                                        this.move = 1;

                                    },200);



                                }

                                setTimeout(()=>{
                                    this.loader.AnimationActions[6].paused=true;
                                    this.punchHard = false;
                                },400);
                            }

                        }






                    });

                    document.addEventListener('keyup', ($event)=>{

                        if($event.key==='d') {

                            this.loader.AnimationActions[1].time = 0;
                            this.moveLeft = false;

                        }

                        if($event.key==='a') {
                            this.loader.AnimationActions[2].time = 0;
                            this.moveRight = false;


                        }

                        if($event.key==='w') {

                            this.jump = false;
                            this.loader.AnimationActions[5].reset();
                        }

                        if ($event.key === 's') {

                            this.loader.AnimationActions[3].time = 0;

                            this.loader.AnimationActions[3].paused = false;


                            this.bend = false;

                        }
                    });
                }

                if(index === 1){

                    this.loader.AnimationActions[5].getMixer().addEventListener( 'finished', ()=>{


                        this.loader.AnimationActions[5].reset();

                    } );


                    this.loader.AnimationActions[4].getMixer().addEventListener( 'finished', ()=>{

                        this.punch = false;

                        this.loader.AnimationActions[4].reset();
                    } );




                    document.addEventListener('keydown', ($event)=>{

                        if($event.key==='ArrowRight') {

                            if(!this.bend) {

                                this.moveLeft = true;

                            }

                        }



                        if($event.key==='ArrowLeft') {

                            if(!this.bend){

                                this.moveRight = true;

                            }



                        }

                        if ($event.key === 'ArrowUp') {

                            if(!this.bend) {

                                this.jump = true;

                            }

                        }

                        if ($event.key === 'ArrowDown') {

                            this.bend = true;


                        }

                        if ($event.key === '1') {


                            if(!this.punch && !this.bend){
                                this.loader.AnimationActions[4].paused=false;
                                this.loader.AnimationActions[4].time = 0;
                                this.punch = true;

                                if(!this.onHitting){

                                    this.onHitting = true;
                                    this.move = 0;

                                }


                                setTimeout(()=>{
                                    this.loader.AnimationActions[4].paused=true;
                                    this.punch= false;
                                    this.onHitting = false;
                                },300);
                            }




                        }

                        if ($event.key === '5') {

                            if(!this.punchHard && !this.bend){
                                this.loader.AnimationActions[6].paused=false;
                                this.loader.AnimationActions[6].time = 0;
                                this.punchHard = true;

                                if(!this.onHitting){

                                    setTimeout(()=>{

                                        this.onHitting = true;
                                        this.move = 1;

                                    },200);



                                }

                                setTimeout(()=>{
                                    this.loader.AnimationActions[6].paused=true;
                                    this.punchHard = false;
                                },400);
                            }

                        }






                    });

                    document.addEventListener('keyup', ($event)=>{

                        if($event.key==='ArrowRight') {

                            this.loader.AnimationActions[1].time = 0;
                            this.moveLeft = false;

                        }

                        if($event.key==='ArrowLeft') {
                            this.loader.AnimationActions[2].time = 0;
                            this.moveRight = false;


                        }

                        if($event.key==='ArrowUp') {

                            this.jump = false;
                            this.loader.AnimationActions[5].reset();
                        }

                        if ($event.key === 'ArrowDown') {

                            this.loader.AnimationActions[3].time = 0;

                            this.loader.AnimationActions[3].paused = false;


                            this.bend = false;

                        }
                    });
                }

            }

        },200);

    }

    handleController($e){


        if($e["BFlag"] === true){

            if(!this.punchHard && !this.bend){
                this.loader.AnimationActions[6].paused=false;
                this.loader.AnimationActions[6].time = 0;
                this.punchHard = true;

                if(!this.onHitting){

                    setTimeout(()=>{

                        this.onHitting = true;
                        this.move = 1;

                    },200);



                }

                setTimeout(()=>{
                    this.loader.AnimationActions[6].paused=true;
                    this.punchHard = false;
                },400);
            }


        }


        if($e["AFlag"] === true){
            if(!this.punch && !this.bend){
                this.loader.AnimationActions[4].paused=false;
                this.loader.AnimationActions[4].time = 0;
                this.punch = true;

                if(!this.onHitting){

                    this.onHitting = true;
                    this.move = 0;

                }


                setTimeout(()=>{
                    this.loader.AnimationActions[4].paused=true;
                    this.punch= false;
                    this.onHitting = false;
                },300);
            }



        }

            this.moveRight = $e["leftFlag"] && !this.bend;




        this.moveLeft = $e["rightFlag"]  && !this.bend;

        if($e["upFlag"]){

            this.jump = !this.bend;

        }
        else{

            this.jump = false;
            //this.loader.AnimationActions[5].reset();

        }


        if($e["botFlag"]){

            this.bend = true;

        }
        else{

            this.loader.AnimationActions[3].time = 0;

            this.loader.AnimationActions[3].paused = false;

            this.bend = false;

        }






    }



    loader;
    rayCaster;
    downRay;
    spotLight;
    physicsHandler;
    lifePercentage;
    index;

    moveLeft;
    moveRight;
    jump;
    bend;
    punch;
    kick;
    kickHard;
    punchHard;

    onHitting;
    onReceiveHit;
    move;

    animation;


    combo0 = false;
    combo1 = false;
    combo2 = false;

}
