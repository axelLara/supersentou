export class ControllerHandler{


    constructor(){


        this.gamepadAPI = {
            controller: {},
            turbo: false,
            connect: (evt) =>{
                this.gamepadAPI.controller = evt.gamepad;
                this.gamepadAPI.turbo = true;
                console.log('Gamepad connected.');

            },
            disconnect: (evt) => {
                this.gamepadAPI.turbo = false;
                delete this.gamepadAPI.controller;
                console.log('Gamepad disconnected.');

            },
            update: ()=> {
                // clear the buttons cache
                this.gamepadAPI.buttonsCache = [];
                // move the buttons status from the previous frame to the cache
                for(var k=0; k<this.gamepadAPI.buttonsStatus.length; k++) {
                    this.gamepadAPI.buttonsCache[k] = this.gamepadAPI.buttonsStatus[k];
                }
                // clear the buttons status
                this.gamepadAPI.buttonsStatus = [];
                // get the gamepad object
                var c = this.gamepadAPI.controller || {};

                // loop through buttons and push the pressed ones to the array
                var pressed = [];
                if(c.buttons) {
                    for(var b=0,t=c.buttons.length; b<t; b++) {
                        if(c.buttons[b].pressed) {
                            pressed.push(this.gamepadAPI.buttons[b]);
                        }
                    }
                }
                // loop through axes and push their values to the array
                var axes = [];
                if(c.axes) {
                    for(var a=0,x=c.axes.length; a<x; a++) {
                        axes.push(c.axes[a].toFixed(2));
                    }
                }
                // assign received values
                this.gamepadAPI.axesStatus = axes;
                this.gamepadAPI.buttonsStatus = pressed;
                // return buttons for debugging purposes
                return pressed;
            },
            buttonPressed: function(button) {


                if (typeof(button) == "object") {
                    return button.pressed;
                }
                return button === 1.0;
            },
            buttons: [],
            buttonsCache: [],
            buttonsStatus: [],
            axesStatus: []
        };





        window.addEventListener("gamepadconnected", this.gamepadAPI.connect);
        window.addEventListener("gamepaddisconnected", this.gamepadAPI.disconnect);


    }

    GetPressedKeys($e){

        let upFlag = false;

        let botFlag = false;

        let leftFlag = false;

        let rightFlag = false;

        let AFlag = false;

        let BFlag = false;


        if(this.gamepads[$e]!==null) {

            const gp1 = this.gamepads[$e];

            if (this.gamepadAPI.buttonPressed(gp1.buttons[0])) {

                AFlag = true;
            }

            if (this.gamepadAPI.buttonPressed(gp1.buttons[1])) {

                BFlag = true;
            }

            /*
            if (this.gamepadAPI.buttonPressed(gp1.buttons[2])) {

                console.log("X1");
            }


            if (this.gamepadAPI.buttonPressed(gp1.buttons[3])) {

                console.log("Y1");
            }

*/

            if (this.gamepadAPI.buttonPressed(gp1.buttons[12])) {

                upFlag = true;
            }

            if (this.gamepadAPI.buttonPressed(gp1.buttons[13])) {

                botFlag = true;
            }

            if (this.gamepadAPI.buttonPressed(gp1.buttons[14])) {

                leftFlag = true;
            }

            if (this.gamepadAPI.buttonPressed(gp1.buttons[15])) {

                rightFlag = true;
            }
        }


        return {

            upFlag: upFlag,

            botFlag: botFlag,

            leftFlag: leftFlag,

            rightFlag: rightFlag,

            AFlag: AFlag,

            BFlag: BFlag

        }
    }

    Update(){

        this.gamepadAPI.update();

        this.gamepads = navigator.getGamepads ? navigator.getGamepads() : (navigator.webkitGetGamepads ? navigator.webkitGetGamepads : []);



    }


    gamepads = [];
    gamepadAPI;
}
