import {playerUI} from "./UI.js";
import {Stage1, Stage2} from "./Stage.js";
import {Character} from "./Character.js";
import {CombatHandler} from "./Moves.js";
import {share} from "./face.js";
import {ControllerHandler} from './Controller.js';




export class GameHandler{


    Render(){



        requestAnimationFrame(this.Render.bind(this));
        let deltaTime = this.clock.getDelta();


        //this.camera.aspect = window.innerWidth / window.innerHeight;
        //this.camera.updateProjectionMatrix();

        this.controllerH.Update();

        if(this.controllerH.gamepads[0]!==null){

            this.player0.handleController(this.controllerH.GetPressedKeys(0));

        }

        if(this.controllerH.gamepads[1]!==null){

            this.player0.handleController(this.controllerH.GetPressedKeys(1));

        }



        this.uiPlayer0.Update();
        this.uiPlayer1.Update();




            this.player0.Render(deltaTime, this.stage.stageHandler.ground);
            this.player1.Render(deltaTime, this.stage.stageHandler.ground);




        if(this.player0.onHitting){


            this.combatHandler.Blow(this.player0.Blow(), this.player0);

        }

        if(this.player1.onHitting){


            this.combatHandler.Blow(this.player1.Blow(), this.player1);

        }

        if(this.player0.loader.parent.position.x>this.player1.loader.parent.position.x){

            this.player0.LookLeft();
            this.player1.LookRight();

        }

        else{

            this.player1.LookLeft();
            this.player0.LookRight();

        }

        this.uiPlayer0.lifeBar.lifePercentage = this.player0.lifePercentage;
        this.uiPlayer1.lifeBar.lifePercentage = this.player1.lifePercentage;

        if(this.player0.lifePercentage <= 0){

            share();

        }

        if(this.player1.lifePercentage <= 0){

            share();

        }

        //this.stage.smoke.animate(deltaTime);  //Solo si es el stage 1

        this.renderer.render(this.scene, this.camera);


    }




    constructor() {
        this.canvas = document.querySelector('canvas');
        this.scene = new THREE.Scene();



        let width = window.innerWidth;
        let height = window.innerHeight;

        let d = 1;
        this.camera = new THREE.PerspectiveCamera(15, width / height, 0.1, 100);

        this.renderer = new THREE.WebGLRenderer({antialias: true});

        this.camera.position.y = 0.7;
        this.camera.position.z = 7.5;


        this.renderer.setClearColor('black');
        this.renderer.setSize(window.innerWidth, window.innerHeight);
        this.renderer.setPixelRatio(width / height);

        let ambientLight = new THREE.AmbientLight(new THREE.Color(0.6, 0.4, 0.6), 0.4);
        this.scene.add(ambientLight);

        let directionalLight = new THREE.DirectionalLight(new THREE.Color(1, 1, 1), 0.2);
        directionalLight.position.set(0, 10, 10);
        this.scene.add(directionalLight);

        document.body.appendChild(this.renderer.domElement);

        this.clock = new THREE.Clock();

/*
        const stage1textures = {

            diffuseFloor: './assets/Textures/Stages/Stage0/diffuse.jpg',
            normalFloor:'./assets/Textures/Stages/Stage0/normal.jpg',

            diffuseWall: './assets/Textures/Stages/Stage0/paredDiffuse.jpg',
            normalWall: './assets/Textures/Stages/Stage0/paredNormal.jpg'

        };

        this.stage = new Stage1(stage1textures, this.scene);
*/

        const stage2textures = {

            diffuseFloor: './assets/Textures/Stages/Stage1/albedo.jpg',
            normalFloor:'./assets/Textures/Stages/Stage1/normal.jpg',

            diffuseWall: './assets/Textures/Stages/Stage1/background.jpg',
            normalWall: './assets/Textures/Stages/Stage0/paredNormal.jpg'

        };


        this.stage = new Stage2(stage2textures, this.scene);

        this.player0 = new Character(0);
        this.scene.add(this.player0.loader.parent);
        this.player0.loader.parent.position.x = -1.2;
        this.player0.loader.parent.scale.x *= -1;



        this.player1 = new Character(1);
        this.scene.add(this.player1.loader.parent);
        this.player1.loader.parent.position.x = 1.2;


        this.uiPlayer0 = new playerUI(
            document.querySelector('#UI0 div.lifePercentage'),
            document.querySelector('#UI0 div.powerPercentage'));



        this.uiPlayer1 = new playerUI(
            document.querySelector('#UI1 div.lifePercentage'),
            document.querySelector('#UI1 div.powerPercentage'));

        this.combatHandler = new CombatHandler(this.player0, this.player1);

        window.addEventListener('resize', ()=>{

            this.renderer.setSize(window.innerWidth, window.innerHeight);

        });


        this.controllerH = new ControllerHandler();


    }

    controllerH
    combatHandler;
    canvas;
    scene;
    renderer;
    camera;
    clock;

    player0;

    uiPlayer0;
    uiPlayer1;

    stage;
}
