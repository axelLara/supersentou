export class Move{

    constructor(range,damage, push) {

        this.range = range;
        this.damage = damage;
        this.push = push;

    }

    range;
    damage;
    push;

}

export class CombatHandler{


    constructor(a,b) {

        this.a = a;
        this.b = b;

    }


    Blow(move, emitter){

        const posA =
                [this.a.loader.parent.position.x,
                this.a.loader.parent.position.y,
                this.a.loader.parent.position.z];

        const posB =
                [this.b.loader.parent.position.x,
                this.b.loader.parent.position.y,
                this.b.loader.parent.position.z];

        const abDist =
            Math.sqrt(
                Math.pow(posB[0]-posA[0],2) +
                Math.pow(posB[1]-posA[1],2) +
                Math.pow(posB[2]-posA[2],2) );

        if(abDist<move.range && Math.abs(this.b.loader.parent.position.y-this.a.loader.parent.position.y)<0.55){


            if(emitter === this.a){

                if(this.a.loader.parent.position.x>this.b.loader.parent.position.x){

                    move.push *= -1;

                    this.b.ReceiveBlow(move);

                }
                else{

                    this.b.ReceiveBlow(move);

                }


            }

            else{

                if(this.a.loader.parent.position.x<this.b.loader.parent.position.x){

                    move.push *= -1;

                    this.a.ReceiveBlow(move);

                }
                else{

                    this.a.ReceiveBlow(move);

                }

            }

        }

    }

    a;
    b;
}
