

export class Physics{


    constructor(){


        this.maxAcceleration = 2.8;

        this.vy = 0;
        this.g = -this.maxAcceleration;
        this.n = -this.g;

        this.startNoCollisionFlag = false;
        this.jumpingFlag = false;
        this.time0 = 0;
        this.time1 = 0;

        this.x = 0;
        this.initAcceleration = 0;
        this.maxFriction = 0.1;
        this.friction = 0;
        this.initialInstant = false;
        this.initAccSign = false;

    }

    Update(deltaTime, groundCollision){

        this.Fall(deltaTime, groundCollision);
        return this.y;
    }


    Fall(deltaTime, groundCollision){



        if(!groundCollision){           //Si no colisiona con el suelo
            this.n=0;                   //La aceleracion normal se vuelve cero
            if(!this.startNoCollisionFlag){      //Si no habia empezado a saltar

                this.startNoCollisionFlag = true;   //Ya empezo a no colisionar
                if(!this.jumpingFlag) {             //Si no ha empezado a saltar
                    this.time0 = this.time1 = deltaTime;
                }
                else{
                    this.time0 = deltaTime;
                    this.time1 += deltaTime*0.5;
                }
            }
            else{

                this.time1 += deltaTime*0.5;


            }

        }
        else{

            if(this.startNoCollisionFlag){                  //Si colisiona, vuelve al estado de equilibrio en el eje y
                this.jumpingFlag = false;
                this.startNoCollisionFlag = false;
                this.n = -this.g;
                this.time0 = this.time1 = 0;
            }


        }

        if(!this.startNoCollisionFlag) {

            this.vy = (this.g+this.n);  //Cuando cae al suelo, la suma de las aceleraciones es 0

        }
        else{

            this.vy = (this.time1-this.time0) * (this.g+this.n); //Cuando cae al suelo, la suma de las aceleraciones es 0

        }


            this.y +=  (this.vy)*0.08;

            if(this.y <0){
                this.y = 0;
            }


        return this.y;
    }

    SetInitialAcceleration(initAcc){

        this.initAcceleration = initAcc;

        this.initialInstant = true;

        this.initAccSign = this.initAcceleration > 0;



    }

    PushX(deltaTime){


        if(!this.initialInstant){


            if(this.initAccSign){

                if(this.initAcceleration<=0){

                    this.friction = 0;
                    this.initAcceleration = 0;

                }

            }
            else{

                if(this.initAcceleration>=0){

                    this.friction = 0;
                    this.initAcceleration = 0;

                }

            }

        }
        else{
            this.initialInstant = false;

                this.friction = this.maxFriction;   //Se le iguala el modulo de la aceleracion de rozamiento máxima

                if(this.initAccSign){

                    this.friction *=-1;

                }



        }


        this.initAcceleration += this.friction*deltaTime;

        this.x = this.initAcceleration;



        return this.x;
    }

    UpwardsImpulse(deltaTime){
        if(!this.startNoCollisionFlag) {


            this.jumpingFlag = true;     //bandera que dice si el jugador esta saltando

            setTimeout(()=>{
                this.n = 10.5;   //normal + el impulso del salto
            },180);

        }
    }


    y;  //posicion en Y

    time0;  //time when something starts happening
    time1;  //time elapsed since something started happening

    startNoCollisionFlag; //Flag for when collision stopped
    jumpingFlag;   //isJumping

    vy;  //y velocity
    g;   //aceleracion de la gravedad. Siempre vale el negativo de maxAcceleration
    n;   /*aceleracion normal. Vale el positivo de maxAcceleration cuando esta en el suelo. Al instante de salto,
          vale más que maxAcceleration. Al */
    maxAcceleration;


    x; //posicion en X

    initialInstant;
    initAcceleration; //initial acceleration when character gets pushed
    initAccSign;
    maxFriction;
    friction;
}
