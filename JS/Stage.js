import {loadOBJWithMTL} from "./Obj.js";
import {Smoke} from "./particle.js";

export class Stage{

    constructor(floorTexture){

        let plane=  new THREE.PlaneGeometry( this.width, this.height );


        let textureFloor =new THREE.TextureLoader().load(floorTexture.diffuseFloor);
        textureFloor.offset.set( 0, 0 );
        textureFloor.wrapS = THREE.RepeatWrapping;
        textureFloor.wrapT = THREE.RepeatWrapping;
        textureFloor.repeat.set( this.width*3, this.height*3*2);

        const normalTextureFloor = new THREE.TextureLoader().load(floorTexture.normalFloor);
        normalTextureFloor.offset.set( 0, 0 );
        normalTextureFloor.wrapS = THREE.RepeatWrapping;
        normalTextureFloor.wrapT = THREE.RepeatWrapping;
        normalTextureFloor.repeat.set( this.width*3, this.height*3*2);

        const floorMaterial = new THREE.MeshToonMaterial({map: textureFloor, side: THREE.SingleSide});
        floorMaterial.normalMap = normalTextureFloor;

        this.ground = new THREE.Mesh(plane, floorMaterial);

        let textureWall =new THREE.TextureLoader().load(floorTexture.diffuseWall);
        textureWall.offset.set( 0, 0 );
        textureWall.wrapS = THREE.RepeatWrapping;
        textureWall.wrapT = THREE.RepeatWrapping;
        textureWall.repeat.set( 4, 3);

        const normalTextureWall = new THREE.TextureLoader().load(floorTexture.normalWall);
        normalTextureWall.offset.set( 0, 0 );
        normalTextureWall.wrapS = THREE.RepeatWrapping;
        normalTextureWall.wrapT = THREE.RepeatWrapping;
        normalTextureWall.repeat.set( 4, 3);



        const wallMaterial = new THREE.ShaderMaterial(
            {
                uniforms:{

                    texture1: { type: 't', value: null },
                    uvOffset : { type : 'v', value : new THREE.Vector2(0,0) }
                },

            side: THREE.SingleSide,
                vertexShader : document.querySelector("#vertex-shader").textContent,
                fragmentShader: document.querySelector("#fragment-shader").textContent
        });
        wallMaterial.uniforms.texture1.value = textureWall;
        wallMaterial.uniforms.uvOffset.value = textureWall.offset;



        this.wall = new THREE.Mesh(plane, wallMaterial);



        this.ground.rotation.x = THREE.Math.degToRad(this.xAngle);
        this.ground.position.x = 0;
        this.ground.position.y = 0;
        this.ground.position.z = 0;
        this.ground.scale.y = 2;


        this.wall.position.z = -2.5;
        this.wall.position.y = 0.5;
        this.wall.scale.y = 2;

        this.light = new THREE.PointLight( 0xffffff,0.8 ,3);

        this.light.position.set( 0, 1, 0 );
}


    width = 8;
    height = 2;
    xAngle = 280;
    wall;
    ground;
    light;
}


export class Stage1{

    constructor(textures, scene){

        this.stageHandler = new Stage(textures);
        scene.add(this.stageHandler.ground);
        scene.add(this.stageHandler.wall);
        scene.add(this.stageHandler.light);

        this.smoke = new Smoke(scene);

        loadOBJWithMTL("./assets/Models/trash/", "ss.obj", "ss.mtl", (object) => {

            object.position.x = -1.8;
            object.position.y = 0.3;
            object.position.z = -2.5;

            object.scale.x = 0.0045;
            object.scale.y = 0.0045;
            object.scale.z = 0.0045;

            object.rotation.x = THREE.Math.degToRad(90);
            object.rotation.y = THREE.Math.degToRad(180);


            scene.add(object);


        });

        loadOBJWithMTL("./assets/Models/bag/", "tri_bag.obj", "tri_bag.mtl", (object) => {

            object.position.x = -1.6;
            object.position.y = 0.30;
            object.position.z = -2.2;

            object.scale.x = 0.045;
            object.scale.y = 0.045;
            object.scale.z = 0.045;

            object.rotation.x = THREE.Math.degToRad(25);
            object.rotation.y = THREE.Math.degToRad(45);



            scene.add(object);


        });

        loadOBJWithMTL("./assets/Models/car/", "car.obj", "car.mtl", (object) => {

            object.position.x = 1.2;
            object.position.y = 0.35;
            object.position.z = -1.1;

            object.scale.x = 0.32;
            object.scale.y = 0.32;
            object.scale.z = 0.32;


            object.rotation.z = THREE.Math.degToRad(90);

            object.rotation.x = THREE.Math.degToRad(-90);





            scene.add(object);


        });

    }

    stageHandler;
    smoke;

}

export class Stage2{

    constructor(textures, scene){

        this.stageHandler = new Stage(textures);
        scene.add(this.stageHandler.ground);
        scene.add(this.stageHandler.wall);
        scene.add(this.stageHandler.light);


        const piramidesPlane=  new THREE.PlaneGeometry( this.width, this.height );

        const piramidesTexture =new THREE.TextureLoader().load("./assets/Textures/Stages/Stage1/mP.png");

        const piramidesMaterial = new THREE.MeshBasicMaterial({map: piramidesTexture, side: THREE.SingleSide,
            transparent: true});

        const piramidesMesh = new THREE.Mesh(piramidesPlane, piramidesMaterial);

        scene.add(piramidesMesh);

        piramidesMesh.position.z = -2;
        piramidesMesh.position.y = 0.45;
        piramidesMesh.position.x = 1.2;
        piramidesMesh.scale.x = 4;
        piramidesMesh.scale.y = 2;


        loadOBJWithMTL("./assets/Models/cactus/", "model.obj", "materials.mtl", (object) => {




            object.rotation.y = THREE.Math.degToRad(-110);
            object.position.x = -1.4;
            object.position.y = 0.38;
            object.position.z = -1.8;

            object.scale.x = 0.6;
            object.scale.y = 0.6;
            object.scale.z = 0.6;


            scene.add(object);


        });

    }

    stageHandler;


}



