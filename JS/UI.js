class uiBar{

    constructor(uiElement){
        this.uiElement=uiElement;
        this.lifePercentage=100;
    }

    Update(){

        this.uiElement.style.width=this.lifePercentage+'%';

    }


    uiElement;
    lifePercentage;
}


export class playerUI{

    constructor(lifeBarSelector, powerBarSelector, charName){
        this.lifeBar  = new uiBar(lifeBarSelector);
        this.powerBar = new uiBar(powerBarSelector);
        this.characterName = charName;
    }

    Update(){
        this.powerBar.Update();
        this.lifeBar.Update();
    }

    characterName;
    lifeBar;
    powerBar;
}






