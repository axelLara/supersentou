export class StartingScreen{

    constructor(buttonSonido, buttonComenzar, texts, subtitle, barraFondo){



        buttonSonido.addEventListener('click', ()=>{

            buttonSonido.style.display = 'none';

            let yoo = new Audio("assets/audio/yoo.mp3");
            yoo.play();
            setTimeout(function(){


                const inter=setInterval(()=>{

                    if((yoo.volume -0.1) >= 0){
                        yoo.volume -=0.1;

                    }
                    else{
                        clearInterval(inter);
                        yoo.volume = 0;
                    }
                },120);

            }, 9000);


            texts[0].style.animation = "tituloCae1 2s";
            texts[1].style.animation = "tituloCae2 2s";
            texts[0].style.transform = "rotateY(0deg) rotateZ(0deg)";
            texts[1].style.transform = "rotateY(0deg) rotateZ(0deg)";


            setTimeout(()=>{
                subtitle.style.animation = "subtitulo 1.5s"
                subtitle.style.opacity = '1';
            },1500);

            setTimeout(()=>{

                for(let i of barraFondo){
                    i.style.animation = "barraFondo 1.5s";
                    i.style.transform = "scaleY(8)";
                }

            },3000);


            setTimeout(()=>{

                subtitle.style.color = "black";
                subtitle.style.animation = "subtitulo2 0.5s";

                for(let i of texts){

                    i.style.animation = "tituloColor 0.5s";
                    i.style.color = "red";
                }

            },3550);

            setTimeout(()=>{

                buttonComenzar.style.display = "flex";

            },7000);
        })
        buttonComenzar.addEventListener('click', ()=>{

            buttonComenzar.style.display = 'none';

            for (let i of barraFondo){
                i.style.animation = "barraFondoFinal 2s";
                setTimeout(()=>{
                    i.style.transform ="scaleY(8)";
                    i.style.height ="24rem";
                    i.style.display ="none";

                    },1900);

            }

            setTimeout(()=>{

                subtitle.style.display = "none";


            },1900);

        })


    }

}
