

export class AudioButton{

    constructor(element, audio){

        this.element = element;
        this.audio = audio;


        this.element.addEventListener('click', ()=>{

            this.audio.play();
            this.time = 0;

        })
    }

    element;
    audio;
}


function ButtonInitializer(){

    const audioOK = new Audio("assets/audio/OK.mp3");
    const audioBACK = new Audio("assets/audio/BACK.mp3");

    const okButtons = document.querySelectorAll("div.button--ok");
    const backButtons = document.querySelectorAll("div.button--back");

    let okAudioButtons = [];

    let backAudioButtons = [];

    for(let i of okButtons){
        backAudioButtons.push(new AudioButton(i, audioOK));
    }
    for(let i of backButtons){
        backAudioButtons.push(new AudioButton(i, audioBACK));
    }

}

ButtonInitializer();
