export class Smoke
{

    constructor(scene) {

        this.texturePath = "./assets/Textures/smoke/smoke.png";

        const loader = new THREE.TextureLoader();

        this.status = false;

        var smokeprt = [];

        loader.load(this.texturePath, function (texture){
            const portalGeo = new THREE.PlaneBufferGeometry(0.2,0.8);
            const portalMaterial = new THREE.MeshStandardMaterial({
                map:texture,
                transparent: true,
                opacity:0.5
            });
                for(let i = 0 ; i<2; ++i){
                let particle = new THREE.Mesh(portalGeo,portalMaterial);
                particle.position.set(0,1,0);

                particle.position.y = 1.5;
                smokeprt.push(particle);

                scene.add(particle);
                }

        });

        this.smokeParticles = smokeprt;

    }

    animate(deltaTime){



            this.smokeParticles[0].position.y += deltaTime/8;

            if(this.smokeParticles[0].position.y >= 2){
                this.smokeParticles[0].position.y = 1;

            }
        this.smokeParticles[1].position.y += deltaTime/4;

        if(this.smokeParticles[1].position.y >= 2){
            this.smokeParticles[1].position.y = 1;

        }



    }

    texturePath;
    smokeParticles = [];
    status;
}
