import {AudioButton} from "./pages/button.js";
import {StartingScreen} from "./pages/StartingScreen.js";

function startMain(){

    const buttonSonido = document.querySelector("div.button--sonido");
    const buttonComenzar = document.querySelector("div.button--comenzar");

    const title = document.querySelector('div.title');

    const texts = document.querySelectorAll("div.text");

    const subtitle = document.querySelector(".subtitle");
    const barraFondo = document.querySelectorAll("div.barraFondo div");

    let startingScreen = new StartingScreen(buttonSonido, buttonComenzar, texts, subtitle, barraFondo);

    const panelPrincipal =  document.querySelector(".panelPrincipal");
    const aside =  panelPrincipal.querySelector("aside");

    const btnCombate= document.querySelector("div.button--combate");
    const btnOpciones = document.querySelector("div.button--opciones");
    const btnPuntuaciones = document.querySelector("div.button--puntuaciones");
    const btnCreditos = document.querySelector("div.button--creditos");





    buttonComenzar.addEventListener('click', ()=>{

        setTimeout(()=>{
            panelPrincipal.style.display = "flex";

            title.style.animation = "";
            title.style.transform = 'translateY(20%)';
            texts[0].style.color = 'black';
            texts[0].style.webkitTextStroke = "1.5px #024bfb";
            texts[1].style.color = 'black';
            texts[1].style.webkitTextStroke = "1.5px #024bfb";
            title.style.fontSize = '6rem';

            texts[0].style.animation = "tituloLevanta 12s";
            texts[0].style.animationIterationCount = "infinite";

            texts[1].style.animation = "tituloLevanta 18s";
            texts[1].style.animationIterationCount = "infinite";

        } , 2100);





    });


    btnCombate.addEventListener('mouseenter', ()=>{
        aside.classList.add("pelea");
        aside.classList.remove("opciones");
        aside.classList.remove("puntuaciones");
        aside.classList.remove("creditos");
    });


    btnPuntuaciones.addEventListener('mouseenter', ()=>{
        aside.classList.add("puntuaciones");
        aside.classList.remove("opciones");
        aside.classList.remove("pelea");
        aside.classList.remove("creditos");
    });

    btnCreditos.addEventListener('mouseenter', ()=>{
        aside.classList.add("creditos");
        aside.classList.remove("opciones");
        aside.classList.remove("puntuaciones");
        aside.classList.remove("pelea");
    });


    const panelPelear = document.querySelector("section.panelPelear");

    btnCombate.addEventListener(('click'),()=>{

        panelPrincipal.style.display = "none";
        panelPelear.style.display = "flex";

    });

    const buttonBackPelear = panelPelear.querySelector("div.button--atras");

    buttonBackPelear.addEventListener("click", ()=>{

        panelPrincipal.style.display = "flex";
        panelPelear.style.display = "none";

    });

    const panelPuntuaciones = document.querySelector("section.panelPuntuaciones");

    btnPuntuaciones.addEventListener(('click'),()=>{

        panelPrincipal.style.display = "none";
        panelPuntuaciones.style.display = "block";

    });

    const buttonBackPuntuaciones = panelPuntuaciones.querySelector("div.button--atras");

    buttonBackPuntuaciones.addEventListener("click", ()=>{

        panelPrincipal.style.display = "flex";
        panelPuntuaciones.style.display = "none";

    });




    const modal = document.querySelector("section.modalSala");



    const buttonCloseModal = document.querySelector("#buttonCloseModal");

    buttonCloseModal.addEventListener("click", ()=>{

       modal.style.display = "none";

    });


    }

startMain();


