function accMain(){

    let id = "0";

    const storage = window.localStorage;

    const root = "http://localhost/Proyecto/PHP/";

    const initButton = document.querySelector("#init");   //Botón para el Inicio

    const peleaButton = document.querySelector("#pelea");   //Guardar configuración

    const escenarioButtons = document.querySelectorAll(".escenario");  //Arreglo para darle funcionalidad a los
                                                                            //botones de escenario.

    const cargarButton = document.querySelector("#cargarConfiguracion");

    let escenario = 0;   //Variable para guardar el escenario seleccionado.

    /*
    Inicio. Si encuentra el ID en el localstorage, pasa normal. Si no lo encuentra, crea un
    registro en la base de datos y guarda en el localstorage el id de aquel registro.
    */
    initButton.addEventListener("click", ()=>{

        id = storage.getItem("user");



        if(id === null || id === undefined || id === ""){

            //AJAX
            fetch( root + "post.php",
                {
                    method: 'POST'
                }
            ).then( res => res.json())
             .then( data => {

                 storage.setItem("user", data.toString());

             });

        }

        else{

            //Por si queremos algo luego...

        }

    });

    /*
    Guardar Configuración
    */

    //Damos funcionalidad a los botones de escenario

    for(let i = 0; i<escenarioButtons.length; ++i){
        escenarioButtons[0].style.fiter = "grayscale(0)";

        escenarioButtons[i].addEventListener("click",()=>{

            for(let j of escenarioButtons){

                j.style.filter = "grayscale(100%)";

            }

            escenarioButtons[i].style.filter = "grayscale(0)";
            escenario = i;

        });

    }

    //llamada http guardar nombre

    peleaButton.addEventListener("click", ()=>{


        const selectHonorifico = document.querySelector("#honorifico");

        //LocalStorage: se guardan las preferencias en el navegador

        const preferences = {

            escenario: escenario,

            nickname: document.querySelector("#nickname").value,

            honorifico: selectHonorifico.options[selectHonorifico.selectedIndex].textContent

        }

        storage.setItem("preferencias", JSON.stringify(preferences));



        //AJAX
        fetch( root + `name.php/?name=${preferences.nickname}&id=${id}`,
            {
                method: 'GET'
            }
        );

    });

    //Cargar configuración

    cargarButton.addEventListener("click", ()=>{

            const preferencias = JSON.parse(storage.getItem("preferencias"));

             document.querySelector("#nickname").value = preferencias.nickname;

             const honorificoSelect = document.querySelector("#honorifico");

             for(let i = 0; i<honorificoSelect.options.length; ++i){

                 if(honorificoSelect.options[i].textContent === preferencias.honorifico){

                     honorificoSelect.selectedIndex = i;
                     break;

                 }

             }

             escenario = preferencias.escenario;

                document.location.href = "./index.html";


    });


}


accMain();
