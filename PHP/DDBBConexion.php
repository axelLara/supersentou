<?php

class Conexion{

    final function __construct() {

        $this->host = "localhost";
        $this->name = "sentou";
        $this->username = "root";
        $this->password = "";
        $this->connection = null;

    }

    public final function Initialize(){

        if($this->connection==null){
            $this->connection = mysqli_connect($this->host, $this->username, $this->password, $this->name);

        }

    }



    public final function Close(){
        if($this->connection!=null){
            mysqli_close($this->connection);

        }

        $this->connection = null;

    }

    private $host;          //direccion del host de la bbdd
    private $name;          //nombre de la bbdd
    private $username;      //nombre de usuario de la bbdd
    private $password;      //contraseña de la bbdd

    public $connection;
}

