USE SENTOU;


CREATE TABLE jugador(
id INT auto_increment PRIMARY KEY,
usuario VARCHAR(255),
contrasena VARCHAR(255)
);


CREATE TABLE partida(
id INT auto_increment PRIMARY KEY,
jugador INT,
rival VARCHAR(255),
FOREIGN KEY (jugador) REFERENCES jugador(id)
);